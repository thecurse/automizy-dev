import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Layout, Row, Col, Spin, Empty, List, Typography, Button, Modal, message, Form, Input, Select } from 'antd'
const { Title } = Typography
const { confirm } = Modal
import { ExclamationCircleOutlined } from '@ant-design/icons'
const { Header, Content } = Layout

const Management = () => {
    const [reloadListTrigger, setReloadListTrigger] = useState(null)
    const [showModal, setShowModal] = useState(false)
    const [students, setStudents] = useState({
        data: null,
        complete: false,
        error: false
    })

    const [projects, setProjects] = useState({
        data: null,
        complete: false,
        error: false
    })

    useEffect(() => {
        loadStudentsAndProjects()
    }, [])

    // Studentek és projektek betöltése a hozzáadáshoz való használathoz
    const loadStudentsAndProjects = async () => {
        axios
            .get('api/student')
            .then((res) => {
                setStudents({
                    data: res.data.students,
                    error: false,
                    complete: true
                })
            })
            .catch(() => {
                setStudents({
                    data: null,
                    error: true,
                    complete: true
                })
            })

        axios
            .get('api/project')
            .then((res) => {
                setProjects({
                    data: res.data.projects,
                    error: false,
                    complete: true
                })
            })
            .catch(() => {
                setProjects({
                    data: null,
                    error: true,
                    complete: true
                })
            })
    }

    const onClickAddNewProject = () => {
        setShowModal(true)
    }
    const onClickCancel = () => {
        setShowModal(false)
    }
    const onDone = ({ name }) => {
        setShowModal(false)
        setReloadListTrigger(new Date().getTime())
        message.success('The following student has been added to the project: ' + name)
    }

    return (
        <Layout>
            <Header className='header'>
                <Row>
                    <Col span={22}>
                        <Title>Management Handler</Title>
                    </Col>
                    <Col span={2}>
                        <Button type='primary' onClick={onClickAddNewProject}>
                            Add Student to Project
                        </Button>
                    </Col>
                </Row>
            </Header>
            <Content className='content'>
                <ListManagement reloadListTrigger={reloadListTrigger} />
                <AddManagementModal visible={showModal} students={students} projects={projects} onClickCancel={onClickCancel} onDone={onDone} />
            </Content>
        </Layout>
    )
}

const ListManagement = ({ reloadListTrigger }) => {
    const [trigger, setTrigger] = useState()
    const [loader, setLoader] = useState(true)

    const [list, setList] = useState({
        data: null,
        complete: false,
        error: false
    })
    // Management betöltése
    useEffect(() => {
        setLoader(true)
        setList({
            data: list.data,
            error: false,
            complete: false
        })
        axios
            .get('api/management')
            .then((res) => {
                setLoader(false)
                setList({
                    data: res.data,
                    error: false,
                    complete: true
                })
            })
            .catch(() => {
                setLoader(false)
                setList({
                    data: null,
                    error: true,
                    complete: true
                })
            })
    }, [trigger, reloadListTrigger])
    // Adott management törlésére kattinttás
    const onClickDeleteManagement = ({ name, id }) => {
        confirm({
            title: 'Are you sure to delete student from the project?',
            icon: <ExclamationCircleOutlined />,
            content: name,
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                deleteManagement({ name: name, id: id })
            },
            onCancel() {}
        })
    }
    // Management törlése
    const deleteManagement = ({ id, name }) => {
        setLoader(true)
        axios
            .delete('api/management/' + id)
            .then((res) => {
                message.success('The student has been removed from the project: ' + name)
                setLoader(false)
                setTrigger(new Date().getTime())
            })
            .catch(() => setLoader(false))
    }

    // Managemenet renderelése, null checkkel
    return (
        <Spin size='large' spinning={loader}>
            <Row style={{ marginTop: 8, marginBottom: 8 }}>
                <Col span={24}>
                    {list.complete &&
                        (list.data && list.data.managements.length ? (
                            <List
                                bordered
                                dataSource={list.data.managements}
                                renderItem={(item) => (
                                    <List.Item>
                                        <Typography.Text strong>{item.student == null ? 'Invalid student' : item.student.first_name + ' ' + item.student.last_name}</Typography.Text>
                                        <Typography.Text>{item.project == null ? 'Invalid project' : item.project.name}</Typography.Text>
                                        <Button
                                            type='primary'
                                            onClick={({ id = item.id, name = item.student == null ? 'Invalid student' : item.student.first_name + ' ' + item.student.last_name }) =>
                                                onClickDeleteManagement({ id: id, name: item.student == null ? 'Invalid student' : item.student.first_name + ' ' + item.student.last_name })
                                            }>
                                            Delete
                                        </Button>
                                    </List.Item>
                                )}
                            />
                        ) : (
                            <Empty />
                        ))}
                </Col>
            </Row>
        </Spin>
    )
}
// Új management felvitele
const AddManagementModal = ({ visible, students, projects, onClickCancel, onDone }) => {
    const [form] = Form.useForm()

    const onClickSave = () => {
        form.validateFields()
            .then((values) => {
                saveManagement({
                    project_id: values.project_id,
                    student_id: values.student_id
                })
            })
            .catch((info) => {
                console.log('Validate Failed:', info)
            })
    }
    // Management mentése
    const saveManagement = ({ project_id, student_id }) => {
        axios
            .post('api/management', {
                project_id,
                student_id
            })
            .then(() => {
                form.resetFields()
                onDone({ name: name })
            })
            .catch((err) => {
                if (err.response.status === 409) {
                    setDuplicationErrorMessage({ name: err.response.data.error })
                } else if (err.response.status === 500) {
                    message.error('The selected student is already added to the selected project.')
                }
            })
    }
    const setDuplicationErrorMessage = ({ name }) => {
        form.setFields([
            {
                name: Object.keys(name)[0],
                errors: ['Alredy exists']
            }
        ])
    }

    return (
        <Modal
            visible={visible}
            title='Add Student to Project'
            onCancel={onClickCancel}
            footer={[
                <Button key='cancel' onClick={onClickCancel}>
                    Cancel
                </Button>,
                <Button key='save' type='primary' onClick={onClickSave}>
                    Save
                </Button>
            ]}>
            <Form form={form} layout='vertical'>
                <Form.Item label={'Select Student'} name='student_id' rules={[{ required: true, message: 'Please type project name!' }]}>
                    <Select
                        showSearch
                        placeholder='Search to Select'
                        optionFilterProp='children'
                        filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                        filterSort={(optionA, optionB) => optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())}>
                        {students.data != null
                            ? students.data.map((item) => (
                                  <Select.Option key={item.id} value={item.id}>
                                      {item.first_name} {item.last_name}
                                  </Select.Option>
                              ))
                            : ''}
                    </Select>
                </Form.Item>
                <Form.Item label={'Select Project'} name='project_id' rules={[{ required: true, message: 'Please type project description!' }]}>
                    <Select
                        showSearch
                        placeholder='Search to Select'
                        optionFilterProp='children'
                        filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                        filterSort={(optionA, optionB) => optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())}>
                        {projects.data != null
                            ? projects.data.map((item) => (
                                  <Select.Option key={item.id} value={item.id}>
                                      {item.name}
                                  </Select.Option>
                              ))
                            : ''}
                    </Select>
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default Management
