import express from 'express'
const router = express()
import student from './student'
import management from './management'
import project from './project'

// Documentation
// https://expressjs.com/en/api.html#router

// Hallgatókat kezelő útvonalak
router.use('/student', student)

// Managementet kezelő útvonalak
router.use('/management', management)

// Projekteket kezelő útvonalak
router.use('/project', project)

export default router
