const ManagementModel = ({ sequelize, DataType }) => {
    const { INTEGER, DATE, NOW } = DataType
    const Management = sequelize.define('managements', {
        id: {
            type: INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        student_id: {
            type: INTEGER
        },
        project_id: {
            type: INTEGER
        },
        createdAt: {
            type: DATE,
            defaultValue: NOW
        }
    })
    return Management
}

export default ManagementModel
