import path from 'path'
import grpc from 'grpc'
const protoLoader = require('@grpc/proto-loader')
import config, { project } from '../../config/service'
import db from '../../microservice/database/connect'
import ManagementModel from '../../microservice/database/model/management'
import ProjectModel from '../../microservice/database/model/project'
import StudentModel from '../../microservice/database/model/student'
const PROTO_PATH = path.join(__dirname, '../../proto/management.proto')

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
})

// Load in our service definition
const managementProto = grpc.loadPackageDefinition(packageDefinition).management
const server = new grpc.Server()

const managementModel = ManagementModel(db)
const studentModel = StudentModel(db)
const projectModel = ProjectModel(db)

studentModel.hasMany(managementModel, { foreignKey: 'student_id' })
managementModel.belongsTo(studentModel, { foreignKey: 'student_id' })

projectModel.hasMany(managementModel, { foreignKey: 'project_id' })
managementModel.belongsTo(projectModel, { foreignKey: 'project_id' })

// Implement the list function
const List = async (call, callback) => {
    //const Op = db.DataType.Op
    //const condition = first_name ? { first_name: { [Op.like]: `%${first_name}%` } } : null;

    // Management listázása adatbázisból
    try {
        //const result = await managementModel.findAll({ where: condition })
        const result = await managementModel.findAll({
            include: [
                {
                    model: projectModel
                },
                {
                    model: studentModel
                }
            ]
        })

        callback(null, { managements: result })
    } catch (err) {
        console.log(err)
        callback({
            code: grpc.status.ABORTED,
            details: 'Aborted'
        })
    }
}
// Implement the insert function
const Create = async (call, callback) => {
    let management = call.request
    console.log(management)
    try {
        // Ha már létezik a management, akkor ne hozza újra létre
        let foundExisting = await managementModel.findAll({
            where: {
                student_id: management.student_id,
                project_id: management.project_id
            }
        })

        if (foundExisting && foundExisting.length != 0) {
            let jsErr = new Error('ALREADY_EXISTS')
            jsErr.code = grpc.status.ALREADY_EXISTS
            jsErr.metadata = dbErrorCollector({ errors: err.errors })
            callback(jsErr)

            return
        }

        let result = await managementModel.create(management)
        callback(null, result)
    } catch (err) {
        switch (err.name) {
            case 'SequelizeUniqueConstraintError':
                let jsErr = new Error('ALREADY_EXISTS')
                jsErr.code = grpc.status.ALREADY_EXISTS
                jsErr.metadata = dbErrorCollector({ errors: err.errors })
                callback(jsErr)
                break
            default:
                callback({
                    code: grpc.status.ABORTED,
                    details: 'ABORTED'
                })
        }
    }
}
// Implement the read function
const Read = async (call, callback) => {
    let id = call.request.id
    // data validation
    // ...
    // Kontakt mentése adatbázisba
    try {
        let result = await managementModel.findByPk(id)
        if (result) {
            callback(null, result)
        } else {
            callback({
                code: grpc.status.NOT_FOUND,
                details: 'Not found'
            })
        }
    } catch (err) {
        callback({
            code: grpc.status.ABORTED,
            details: 'Aborted'
        })
    }
}
// Implement the update function
const Update = async (call, callback) => {
    let management = call.request

    try {
        let affectedRows = await managementModel.update(
            {
                student_id: management.student_id,
                project_id: management.project_id
            },
            {
                where: { id: management.id }
            }
        )
        if (affectedRows[0]) {
            callback(null, affectedRows)
        } else {
            callback({
                code: grpc.status.NOT_FOUND,
                details: 'Not found'
            })
        }
    } catch (err) {
        callback({
            code: grpc.status.ABORTED,
            details: 'Aborted'
        })
    }
}
// Implement the delete function
const Delete = async (call, callback) => {
    let id = call.request.id
    try {
        let result = await managementModel.destroy({ where: { id: id } })
        if (result) {
            callback(null, result)
        } else {
            callback({
                code: grpc.status.NOT_FOUND,
                details: 'Not found'
            })
        }
    } catch (err) {
        callback({
            code: grpc.status.ABORTED,
            details: 'Aborted'
        })
    }
}
// Collect errors
const dbErrorCollector = ({ errors }) => {
    const metadata = new grpc.Metadata()
    const error = errors.map((item) => {
        metadata.set(item.path, item.type)
    })
    return metadata
}
const exposedFunctions = {
    List,
    Create,
    Read,
    Update,
    Delete
}

server.addService(managementProto.ManagementService.service, exposedFunctions)
server.bind(config.management.host + ':' + config.management.port, grpc.ServerCredentials.createInsecure())

db.sequelize
    .sync()
    .then(() => {
        console.log('Re-sync db.')
        server.start()
        console.log('Server running at ' + config.management.host + ':' + config.management.port)
    })
    .catch((err) => {
        console.log('Can not start server at ' + config.management.host + ':' + config.management.port)
        console.log(err)
    })
